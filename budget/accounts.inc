account assets:SPI

account expenses:travel:bursary:diversity
account expenses:travel:bursary:general
account expenses:travel:team:debconf-ctte
account expenses:travel:team:dc24
account expenses:travel:team:registration
account expenses:travel:team:video
account expenses:social:day trip
account expenses:social:day trip:bus
account expenses:fees:bank
account expenses:fees:SPI
account expenses:food:debcamp
account expenses:food:debconf
account expenses:food:refreshments
account expenses:general
account expenses:graphic materials
account expenses:incidentals
account expenses:local team
account expenses:party:cheese and wine
account expenses:party:conference dinner:bus
account expenses:party:conference dinner:food
account expenses:party:conference dinner:music
account expenses:party:social event:drinks
account expenses:party:social event:food
account expenses:venue:hygiene
account expenses:venue:av equipment
account expenses:video:computers
account expenses:video:shipping
account expenses:network:equipment
account expenses:network:leased_line
account expenses:social:barbecue
account expenses:video:av-equipment
account expenses:covid:N95_mask
account expenses:covid:N95_mask
account expenses:covid:testing_kit
account expenses:covid:sanitizers
account expenses:local_team
account expenses:misc:childcare
account expenses:misc:tea_bar
account expenses:social:conference_dinner
account expenses:video:import_tax
account expenses:social:c&w:hall
account expenses:social:c&w:liquor_license
account expenses:social:c&w:purchases
account expenses:venue:debcamp:food_accomodation
account expenses:venue:debconf:food_accomodation
account expenses:venue:debcamp:halls
account expenses:venue:debconf:halls
account expenses:swag:merchandise
account expenses:misc:design
account expenses:promotion

account income:hacklab-bar
account income:accommodation
account income:registration
account income:sponsors:bronze
account income:sponsors:gold
account income:sponsors:platinum
account income:sponsors:silver
account income:sponsors:supporter
account income:sponsors:donations

; Create liabilility accounts for orga incurring expenses here
